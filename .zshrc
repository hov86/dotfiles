# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load homebrew
eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/john/.oh-my-zsh"
ZSH_THEME="powerlevel10k/powerlevel10k"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="mm/dd/yyyy"

plugins=(
  git
  vscode
  zsh-autosuggestions
  zsh-syntax-highlighting
  # zsh-completions
)

source $ZSH/oh-my-zsh.sh
# source $ZSH/plugins/zsh-autocomplete.plugin.zsh


# random aliases
alias zshconfig="code ~/.zshrc"
alias ll="exa --long --all"
alias ip='echo "IP Address:" $(ifconfig wlp61s0 | rg -o "\b\d\d\d\S\d\d\d\S\d\S\d\d" | head -1)'
alias deepinconfig="code ~/.config/deepin/deepin-terminal/config.conf"
alias alaconfig="code ~/.config/alacritty/alacritty.yml"
alias kittyconfig="code ~/.config/kitty/kitty.conf"
alias ohmyzsh="code ~/.oh-my-zsh"
alias update="sudo apt-get update -y && sudo apt-get upgrade -y --allow-downgrades && sudo apt autoremove -y && brew update && brew upgrade && brew upgrade --cask && brew cleanup && omz update && tldr --update"

# kube
alias ka='kubectl get all --all-namespaces'
alias kae='kubectl get all --all-namespaces -o wide'
alias kaee='kubectl get all --all-namespaces -o wide --show-labels'
alias kd='kp | grep " [0-9]h" ; kp | grep " [0-9]s"'
alias ki='kubectl get ing --all-namespaces'
alias kiw='watch -n 1 "kubectl get ing --all-namespaces"'
alias kie='kubectl get ing --all-namespaces -o wide'
alias kiee='kubectl get ing --all-namespaces -o wide --show-labels'
alias kkp='kubectl patch pod -p "{"metadata":{"finalizers":null}}" -n '
alias kn='kubectl get nodes'
alias knw='watch -n 1 kubectl get nodes'
alias kne='kubectl get nodes -o wide'
alias knee='kubectl get nodes -o wide --show-labels'
alias kneew='watch -n1 "kubectl get nodes -o wide --show-labels"'
alias kns='kubectl get namespaces --show-labels'
alias kp='kubectl get pods --all-namespaces'
alias kpw='watch -n 1 "kubectl get pods --all-namespaces"'
alias kpe='kubectl get pods --all-namespaces -o wide'
alias kpee='kubectl get pods --all-namespaces -o wide --show-labels'
alias ks='kubectl get svc --all-namespaces'
alias ksw='watch -n 1 "kubectl get svc --all-namespaces"'
alias ksa='kubectl get serviceaccounts --all-namespaces'
alias kse='kubectl get svc --all-namespaces -o wide'
alias ksee='kubectl get svc --all-namespaces -o wide --show-labels'
alias ksm='kubectl get servicemonitors --all-namespaces'
alias ktest='kubectl run -it foo --image=debian:stable-slim --restart=Never -- /bin/bash'
alias kubeconfig-context-get='kubectl config get-contexts'
alias kubeconfig-context-set='kubectl config use-context'
alias kubeconfig-print='kubectl config view --raw --minify=true'
alias kpn='watch -n 1 "kubectl get pods --all-namespaces | grep 0\/ | grep -v Completed"'

# terraform
alias tp='terraform plan'
alias ta='terraform apply'
alias ti='terraform init -upgrade'
alias td='terraform destroy'

# docker
alias di='docker images'
alias dp='docker ps --all'
alias dc='docker container prune -f && docker network prune -f && docker volume prune -f'
alias dca='docker system prune --volumes --all'
alias dn='docker volume ls'
alias dv='docker network ls'
alias dcu='docker-compose up -d'
alias dcd='docker-compose down'
alias dcp='docker-compose pull --include-deps'
alias dcr='docker-compose restart'
alias dl='docker network ls && echo && docker image ls --all && echo && docker container ls --all && echo && docker volume ls'

# vagrant
alias vu='vagrant up'
alias vh='vagrant halt'
alias vp='vagrant provision'
alias vr='vagrant reload'
alias vd='vagrant destroy -f'
alias vs='vagrant status'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias fd='fdfind'
alias python='/usr/bin/python3'


# ZSH autocomplete settings
zstyle ':completion:*' verbose no
zstyle ':autocomplete:*' min-input 3

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/packer packer
export PATH="/usr/local/sbin:$PATH"
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# ----------------------------------
# Colors
# ----------------------------------
NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

echo -e "Hi $(echo $USER | sed 's/\([a-z]\)\([a-zA-Z0-9]*\)/\u\1\2/g')! Today is ${BLUE}$(date +"%A %B %d, %G")${NOCOLOR} and it is now ${BLUE}$(date +"%I:%M %p")${NOCOLOR}"